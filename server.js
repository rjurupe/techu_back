var express = require('express');
var bodyParser = require("body-parser");
var app = express();
var port=process.env.Port|| 3000;
const URL_BASE='/techu-peru/v1/';
var user_file=require('./user.json');
app.use(bodyParser.json());

//var curso_file=require('./curso.json');



//Peticion get

app.get(URL_BASE+'users', function (req, res) {
  //res.send({"msg":"Operacion GET exitosa"});
  res.status(200);
  res.send(user_file);

});

//app.listen(3000);

//Peticiòn GET users con ID

app.get(URL_BASE+'users/:id',
 function(req,res){
   let pos=req.params.id -1;
   console.log('GET con id= ' + req.params.id);
   let tam=user_file.lenght;
   console.log(tam);
   let respuesta=(user_file[pos]==undefined)?{"msg":"Usuario no encontrado"}:user_file[pos];
   let status = (user_file[pos]==undefined)?400:200;
   res.status(status);
   res.send(respuesta);
 });

// GET users con Query String
// app.get(URL_BASE+'users',
// function(req,res){
//   console.log('GET con Query String');
//   console.log(req.query.id) ;
//   console.log(req.query.name);
// });

//post users
app.post(URL_BASE+'users',
function(req,res){
  console.log('Post de Users');
  // console.log('Nuevo   usuario: '+req.body);
  // onsole.log('Nuevo   usuario: '+req.body.first_name);
  // onsole.log('Nuevo   usuario: '+req.body.email);
let newID=user_file.length+1;
  let newUser={
    "id":newID,
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "password": req.body.password
  }

  user_file.push(newUser);
  console.log('nuevo usuario: '+newUser);
  res.send(newUser);
//  res.send({"msg":"Post exitoso"});
});

//Prueba con el put
app.put(URL_BASE+'users/:id',
function(req,res){
  console.log('Put de users');
  let posi=req.params.id -1;
  let userMod={
    "id":req.params.id,
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "password":req.body.password
  }

  //user_file[req.body.id+1]=userMod;
  user_file[posi]=userMod;
  console.log('usuario modificado' + req.body.id);
  res.send(userMod);
});

app.delete(URL_BASE+'users/:id',
function(req,res){
  console.log('Delete users');
  let pos=req.params.id;
  user_file.splice(pos-1,1);
  res.send(user_file[pos]==undefined?{"msg":"No existe posiciòn"}:{"msg":"Usuario Eliminado"});
});


//Login array

// app.post(URL_BASE+'login',
// function(req,res){
// console.log('Login');
// console.log(req.body.email);
// console.log(req.body.password);
// let tam=user_file.length;
// let encontrado=false;
// let i=0;
// while ( i<user_file.length && (!encontrado) )
// {
//   if ((user_file[i].email==req.body.email) && (user_file[i].password==req.body.password))
//   {   encontrado=true;
//      console.log('entro: '+i);
//      user_file[i].logged=true;
//   }
//
//   i++;
//   console.log('nuevo i'+i);
//
// };
//
// //res.send({"msg":"xxx"});
// res.send(!encontrado?{"msg":"No existe usuario"}:{"msg":"Usuario ingresa al aplicativo"});
// });

app.post(URL_BASE+'logout',
function(req,res){
console.log('entrando a logout');
let j=0;
let deslogueado=false;
let msg='';

while (j<user_file.length && (!deslogueado))
{
  if(user_file[j].logged==undefined){
    console.log('no se encuentra logeado'+user_file[j].email);}
  else {
    if ( user_file[j].logged==true && user_file[j].email==req.body.email){
       deslogueado=true;
       console.log('es usuario solicitado');
       user_file[j].logged=false;}
    else
      {console.log('no es usuario solicitado'+user_file[j].email);}
    }
  j++;
}
res.send(!deslogueado?{"msg":"No se encuentra logueado"}:{"msg":"Usuario sale del aplicativo"});
});

app.get (URL_BASE+'userslogueados',
function(req,res){
  let j=0;
  let deslogueado=false;
  let msg='';
  let arrayusu=[{}];

  while (j<user_file.length)
  {
    if(user_file[j].logged!=undefined){
      if ( user_file[j].logged==true){
         arrayusu.push(user_file[j]);
         console.log('es usuario logueado');
         }
       }
    j++;
  }
  res.send(arrayusu);
});

app.post(URL_BASE+'login',
function(req,res){
console.log('Login');
let email=req.body.email;
let password=req.body.password;
let tam=user_file.length;
let encontrado=false;
let i=0;
let arraylogueados=[{}];

while ( i<user_file.length && (!encontrado) )
{
  if ((user_file[i].email==email) && (user_file[i].password==password))
  {  encontrado=true;
     console.log('entro: '+email);
     user_file[i].logged=true;
    // arraylogueados.push(user_file[i]);
    // writeUserDataToFile(arraylogueados);
     writeUserDataToFile(user_file);
  }

  i++;
  console.log('nuevo i'+ email);
};

//res.send({"msg":"xxx"});
res.send(!encontrado?{"msg":"No existe usuario"}:{"msg":"Usuario ingresa al aplicativo"});
});

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }


app.listen(port, function () {
  console.log('Example app listening on port 3000!');
});
